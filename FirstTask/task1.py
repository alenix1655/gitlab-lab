import os
import ast

def merge(first_array, second_array):
    return [item for item in first_array if item in second_array]

if __name__ == "__main__":
    first_array_literal, second_array_literal = os.environ.get("ARRAY1"), os.environ.get("ARRAY2")
    first_array, second_array = ast.literal_eval(first_array_literal) if first_array_literal else [], \
                                ast.literal_eval(second_array_literal) if second_array_literal else []
    print(f"First array: {first_array}")
    print(f"Second array: {second_array}")
    print(f"Merged arrays: {merge(first_array, second_array)}")
    