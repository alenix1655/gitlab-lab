import psycopg2


if __name__ == "__main__":
    conn = psycopg2.connect(database="students", user="postgres",
                        password="postgres", host="postgres", port=5432)
    cursor = conn.cursor()
    
    cursor.execute("""
                   SELECT professor.first_name, professor.middle_name, professor.last_name
                        FROM student_record INNER JOIN professor ON student_record.professor_id=professor.id;
                   """)
    
    records = cursor.fetchall()
    
    for fio in records:
        print(f"Преподаватель {fio[0]} {fio[1]} {fio[2]}")
