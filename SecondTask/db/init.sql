CREATE TABLE student (
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    date_birthday DATE
);

CREATE TABLE subject (
    id SERIAL PRIMARY KEY,
    subject_name VARCHAR(100) NOT NULL
);

CREATE TABLE professor (
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    middle_name VARCHAR(50),
    last_name VARCHAR(50) NOT NULL
);

CREATE TABLE student_record (
    id SERIAL PRIMARY KEY,
    student_id INT REFERENCES student(id),
    subject_id INT REFERENCES subject(id),
    exam_date DATE,
    grade FLOAT,
    professor_id INT REFERENCES professor(id)
);


INSERT INTO student (first_name, last_name, date_birthday)
VALUES
  ('Иван', 'Иванов', '2002-03-15'),
  ('Анастасия', 'Сидорова', '2003-02-21'),
  ('Александр', 'Сафронов', '2001-01-31'),
  ('Анастасия', 'Волкова', '2002-06-02'),
  ('Анастасия', 'Шеметова', '2001-12-06'),
  ('Владислав', 'Елизаров', '2002-04-25'),
  ('Даниил', 'Горелов', '2002-05-29'),
  ('Ваня', 'Ваньков', '2002-09-05'),
  ('Дарья', 'Потемкина', '2002-11-18');
  
INSERT INTO subject (subject_name)
VALUES
  ('Математический анализ'),
  ('Анализ защищенности'),
  ('Кроссплатформенные технологии'),
  ('Проектирование базы данных'),
  ('Алгоритмы и структуры данных');


INSERT INTO professor (first_name, middle_name, last_name)
VALUES
  ('Юрий', 'Михайлович', 'Федоров'),
  ('Константин', 'Юрьевич', 'Попов'),
  ('Сергей', 'Сергеевич', 'Иванов'),
  ('Игоерь', 'Юрьевсосич', 'Лихутвин');

INSERT INTO student_record (student_id, subject_id, exam_date, grade, professor_id)
VALUES
  (1, 1, '2023-01-11', 4, 1),
  (1, 2, '2023-01-14', 3, 2),
  (2, 1, '2023-01-19', 4, 1),
  (2, 2, '2023-01-10', 3, 2),
  (4, 1, '2023-01-10', 4, 1),
  (4, 2, '2023-01-11', 3, 2),
  (4, 3, '2023-01-12', 2, 3),
  (5, 1, '2023-01-19', 4, 1),
  (5, 2, '2023-01-14', 3, 2),
  (5, 3, '2023-01-25', 4, 3),
  (6, 1, '2023-01-30', 4, 1),
  (6, 2, '2023-01-27', 3, 2),
  (7, 1, '2023-01-13', 4, 1),
  (8, 1, '2023-01-12', 4, 1),
  (9, 1, '2023-01-11', 2, 1),
  (9, 2, '2023-01-27', 3, 2);
